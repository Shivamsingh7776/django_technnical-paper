## What is ondelete Cascade?
This is connection between two databases using foreign if we create something one database then second database take the referanse from the first and create some record of first database user. if delete the record from first database then data will be deteled from second automatically because both are connected with foreign key.

## A broad understanding of Fields and Validators available to you
### fields:

### null

### field.null:
If True, Django will store empty values as NULL in the database. Default is False.

Avoid using null on string-based fields such as CharField and TextField. If a string-based field has null=True, that means it has two possible values for “no data”: NULL, and the empty string. In most cases, it’s redundant to have two possible values for “no data;” the Django convention is to use the empty string, not NULL. One exception is when a CharField has both unique=True and blank=True set. In this situation, null=True is required to avoid unique constraint violations when saving multiple objects with blank values.

### blank
### Field.blank:
if True, the field is allowed to be blank. Default is False.

Note that this is different than null. null is purely database-related, whereas blank is validation-related. If a field has blank=True, form validation will allow entry of an empty value. If a field has blank=False, the field will be required.

### choices
### Field.choices:
A sequence consisting itself of iterables of exactly two items (e.g. [(A, B), (A, B) ...]) to use as choices for this field. If choices are given, they’re enforced by model validation and the default form widget will be a select box with these choices instead of the standard text field.

The first element in each tuple is the actual value to be set on the model, and the second element is the human-readable name.

**Example**
```python
YEAR_IN_SCHOOL_CHOICES = [
    ('FR', 'Freshman'),
    ('SO', 'Sophomore'),
    ('JR', 'Junior'),
    ('SR', 'Senior'),
    ('GR', 'Graduate'),
]

```
Generally, it’s best to define choices inside a model class, and to define a suitably-named constant for each value:

```python
from django.db import models

class Student(models.Model):
    FRESHMAN = 'FR'
    SOPHOMORE = 'SO'
    JUNIOR = 'JR'
    SENIOR = 'SR'
    GRADUATE = 'GR'
    YEAR_IN_SCHOOL_CHOICES = [
        (FRESHMAN, 'Freshman'),
        (SOPHOMORE, 'Sophomore'),
        (JUNIOR, 'Junior'),
        (SENIOR, 'Senior'),
        (GRADUATE, 'Graduate'),
    ]
    year_in_school = models.CharField(
        max_length=2,
        choices=YEAR_IN_SCHOOL_CHOICES,
        default=FRESHMAN,
    )

    def is_upperclass(self):
        return self.year_in_school in {self.JUNIOR, self.SENIOR}
```


## Validators:

### Writing validators:
A validator is a callable that takes a value and raises a ValidationError if it doesn’t meet some criteria. Validators can be useful for re-using validation logic between different types of fields.

**Example**

```python
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_even(value):
    if value % 2 != 0:
        raise ValidationError(
            _('%(value)s is not an even number'),
            params={'value': value},
        )
```

You can add this to a model field via the field’s validators argument:

```python
from django.db import models

class MyModel(models.Model):
    even_field = models.IntegerField(validators=[validate_even])
```

Because values are converted to Python before validators are run, you can even use the same validator with forms

```python
from django import forms

class MyForm(forms.Form):
    even_field = forms.IntegerField(validators=[validate_even])
```

### RegexValidator:
A RegexValidator searches the provided value for a given regular expression with re.search(). By default, raises a ValidationError with message and code if a match is not found. Its behavior can be inverted by setting inverse_match to True, in which case the ValidationError is raised when a match is found.

**Syntax**
```
class RegexValidator(regex=None, message=None, code=None, inverse_match=None, flags=0)
```
* regex – If not None, overrides regex. Can be a regular expression string or a pre-compiled regular expression.

* message – If not None, overrides message.

* code – If not None, overrides code.

* inverse_match – If not None, overrides inverse_match.

* flags – If not None, overrides flags. In that case, regex must be a regular expression string, or TypeError is raised.

#### regex:
The regular expression pattern to search for within the provided value, using re.search(). This may be a string or a pre-compiled regular expression created with re.compile(). Defaults to the empty string, which will be found in every possible value.

#### message:
The error message used by ValidationError if validation fails. Defaults to "Enter a valid value".

#### code:
The error code used by ValidationError if validation .

#### inverse_match:
The match mode for regex. Defaults to False.

#### flags:
The regex flags used when compiling the regular expression string regex. If regex is a pre-compiled regular expression, and flags is overridden, TypeError is raised. Defaults to 0.


### EmailValidator:
**Syntax**
```python
class EmailValidator(message=None, code=None, allowlist=None)
```
#### Parameters:	

* message – If not None, overrides message.

* code – If not None, overrides code.

* allowlist – If not None, overrides allowlist.

#### message:
The error message used by ValidationError if validation fails. Defaults to "Enter a valid email address".

#### code:

The error code used by ValidationError if validation fails. Defaults to "invalid".

#### allowlist:

Allowlist of email domains. By default, a regular expression (the domain_regex attribute) is used to validate whatever appears after the @ sign. However, if that string appears in the allowlist, this validation is bypassed. If not provided, the default allowlist is ['localhost']. Other domains that don’t contain a dot won’t pass validation, so you’d need to add them to the allowlist as necessary.

### URLValidator:

**Syntax**

```python
class URLValidator(schemes=None, regex=None, message=None, code=None)
```
A RegexValidator subclass that ensures a value looks like a URL, and raises an error code of 'invalid' if it doesn’t.

#### schemes:
URL/URI scheme list to validate against. If not provided, the default list is ['http', 'https', 'ftp', 'ftps']. As a reference, the IANA website provides a full list of valid URI schemes.


### int_list_validator:

**Syntax**
```python
int_list_validator(sep=',', message=None, code='invalid', allow_negative=False)
```
Returns a RegexValidator instance that ensures a string consists of integers separated by sep. It allows negative integers when allow_negative is True.

### MaxValueValidator:
**Syntax**
```python
class MaxValueValidator(limit_value, message=None)
```

### MinValueValidator:

**Syntax***

```python
class MinValueValidator(limit_value, message=None)
```
Raises a ValidationError with a code of 'min_value' if value is less than limit_value, which may be a callable.

### MaxLengthValidator:

**Syntax**

```python
class MaxLengthValidator(limit_value, message=None)
```
Raises a ValidationError with a code of 'max_length' if the length of value is greater than limit_value, which may be a callable.

### MinLengthValidator:
**Syntax**

```python
class MinLengthValidator(limit_value, message=None)
```
Raises a ValidationError with a code of 'min_length' if the length of value is less than limit_value

### DecimalValidator:

**Syntax**

```python
class DecimalValidator(max_digits, decimal_places)
```
Raises ValidationError with the following codes:
* 'max_digits' if the number of digits is larger than max_digits.

* 'max_decimal_places' if the number of decimals is larger than decimal_places.

* 'max_whole_digits' if the number of whole digits is larger than the difference between max_digits and decimal_places.

### FileExtensionValidator:
**Syntax**
```python
class FileExtensionValidator(allowed_extensions, message, code)
```

### Understanding the difference between Python module and Python class?
There are huge differences between classes and modules in Python.

Classes are blueprints that allow you to create instances with attributes and bound functionality. Classes support inheritance, metaclasses, and descriptors.

Modules can't do any of this, modules are essentially singleton instances of an internal module class, and all their globals are attributes on the module instance. You can manipulate those attributes as needed (add, remove and update), but take into account that these still form the global namespace for all code defined in that module.
