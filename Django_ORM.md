## Using ORM queries in Django Shell
Django lets us interact with its database models, i.e. add, delete, modify and query objects, using a database-abstraction API called ORM(Object Relational Mapper). 

For demonstration purposes, we will use the following Django models.

```python
class Album(models.Model):
    title = models.CharField(max_length = 30)
    artist = models.CharField(max_length = 30)
    genre = models.CharField(max_length = 30)
  
    def __str__(self):
        return self.title
  
class Song(models.Model):
    name = models.CharField(max_length = 100)
    album = models.ForeignKey(Album, on_delete = models.CASCADE)
  
    def __str__(self):
        return self.name
```
We can access the Django ORM by running the following command inside our project directory.

```python
python manage.py shell
```

This brings us to an interactive Python console. Assuming that our models exist in myProject/albums/models.py we can import our models using the following command:

```python
>>> from books.models import Song, Album
```
### Adding objects:
To create an object of model Album and save it into the database, we need to write the following command:
```python
>>> a = Album(title = "Divide", artist = "Ed Sheeran", genre = "Pop")
>>> a.save()
```
To create an object of model Song and save it into the database, we need to write the following command:

```python
>>> s = Song(name = "Castle on the Hill", album = a)
>>> s.save()
```

### Retrieving objects
Let us add 2 more Albums records for the sake of demonstration.

```python
>>> a = Album(title = "Abbey Road", artist = "The Beatles", genre = "Rock")
>>> a.save()
>>> a = Album(title = "Revolver", artist = "The Beatles", genre = "Rock")
>>> a.save()
```
To retrieve all the objects of a model, we write the following command:

```python
>>> Album.objects.all()
<QuerySet [<Album: Divide>, <Album: Abbey Road>, <Album: Revolver>]>
```
We can also filter queries using the functions filter(), exclude() and get(). The filter() function returns a QuerySet having objects that match the given lookup parameters.

```python
>>> Album.objects.filter(artist = "The Beatles")
<QuerySet [<Album: Abbey Road>, <Album: Revolver>]>
```

The exclude() function returns a QuerySet having objects other than those matching the given lookup parameters.

```python
>>> Album.objects.exclude(genre = "Rock")
<QuerySet [<Album: Divide>]>
```
The get() function returns a single object which matches the given lookup parameter. It gives an error when the query returns multiple objects.

```python
>>> Album.objects.get(pk = 3)
<QuerySet [<Album: Revolver>]>
```

### Modifying existing objects:
We can modify an existing object as follows:

```python
>>> a = Album.objects.get(pk = 3)
>>> a.genre = "Pop"
>>> a.save()
```

### Deleting objects:

To delete a single object, we need to write the following commands:

```python
>>> a = Album.objects.get(pk = 2)
>>> a.delete()
>>> Album.objects.all()
<QuerySet [<Album: Divide>, <Album: Revolver>]>
```

To delete multiple objects, we can use filter() or exclude() functions as follows:

```python
>>> Album.objects.filter(genre = "Pop").delete()
>>> Album.objects.all()
<QuerySet []>
```

## What are Aggregations?
The topic guide on Django’s database-abstraction API described the way that you can use Django queries that create, retrieve, update and delete individual objects. However, sometimes you will need to retrieve values that are derived by summarizing or aggregating a collection of objects. This topic guide describes the ways that aggregate values can be generated and returned using Django queries.

```python
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()

class Publisher(models.Model):
    name = models.CharField(max_length=300)

class Book(models.Model):
    name = models.CharField(max_length=300)
    pages = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    rating = models.FloatField()
    authors = models.ManyToManyField(Author)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    pubdate = models.DateField()

class Store(models.Model):
    name = models.CharField(max_length=300)
    books = models.ManyToManyField(Book)
```

```python
# Total number of books.
>>> Book.objects.count()
2452

# Total number of books with publisher=BaloneyPress
>>> Book.objects.filter(publisher__name='BaloneyPress').count()
73

# Average price across all books.
>>> from django.db.models import Avg
>>> Book.objects.aggregate(Avg('price'))
{'price__avg': 34.35}

# Max price across all books.
>>> from django.db.models import Max
>>> Book.objects.aggregate(Max('price'))
{'price__max': Decimal('81.20')}

# Difference between the highest priced book and the average price of all books.
>>> from django.db.models import FloatField
>>> Book.objects.aggregate(
...     price_diff=Max('price', output_field=FloatField()) - Avg('price'))
{'price_diff': 46.85}

# All the following queries involve traversing the Book<->Publisher
# foreign key relationship backwards.

# Each publisher, each with a count of books as a "num_books" attribute.
>>> from django.db.models import Count
>>> pubs = Publisher.objects.annotate(num_books=Count('book'))
>>> pubs
<QuerySet [<Publisher: BaloneyPress>, <Publisher: SalamiPress>, ...]>
>>> pubs[0].num_books
73

# Each publisher, with a separate count of books with a rating above and below 5
>>> from django.db.models import Q
>>> above_5 = Count('book', filter=Q(book__rating__gt=5))
>>> below_5 = Count('book', filter=Q(book__rating__lte=5))
>>> pubs = Publisher.objects.annotate(below_5=below_5).annotate(above_5=above_5)
>>> pubs[0].above_5
23
>>> pubs[0].below_5
12

# The top 5 publishers, in order by number of books.
>>> pubs = Publisher.objects.annotate(num_books=Count('book')).order_by('-num_books')[:5]
>>> pubs[0].num_books
1323
```

### What are Annotations?
Each argument to annotate() is an annotation that will be added to each object in the QuerySet that is returned. The aggregation functions that are provided by Django are described in Aggregation Functions below. Annotations specified using keyword arguments will use the keyword as the alias for the annotation.


### What is a migration file? Why is it needed?
Migrations are Django’s way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your database schema. They’re designed to be mostly automatic, but you’ll need to know when to make migrations, when to run them, and the common problems you might run into.

### The Commands:
There are several commands which you will use to interact with migrations and Django’s handling of database schema:

* migrate, which is responsible for applying and unapplying migrations.
* makemigrations, which is responsible for creating new migrations based on the changes you have made to your models.
* sqlmigrate, which displays the SQL statements for a migration.
* showmigrations, which lists a project’s migrations and their status.


### What are SQL transactions? (non ORM concept)
Transactions group a set of tasks into a single execution unit. Each transaction begins with a specific task and ends when all the tasks in the group successfully complete. If any of the tasks fail, the transaction fails. Therefore, a transaction has only two results: success or failure. 

**Example**
1. read(A)
2. A:=A-150
3. write(A)
4. read(B)
5. B:=B+150
6. write(B)

### What are atomic transactions?
Atomicity is the defining property of database transactions. atomic allows us to create a block of code within which the atomicity on the database is guaranteed. If the block of code is successfully completed, the changes are committed to the database. If there is an exception, the changes are rolled back.

